package types

type Playlist struct {
	Title         string  `json:"title"`
	Id            string  `json:"playlistId"`
	Description   string  `json:"description"`
	Author        string  `json:"author"`
	AuthorId      string  `json:"authorId"`
	VideoCount    int     `json:"videoCount"`
	Videos        []Video `json:"videos"`
}