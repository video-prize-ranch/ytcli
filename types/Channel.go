package types

type Channel struct {
	Name          string  `json:"author"`
	ChannelId     string  `json:"authorId"`
	Description   string  `json:"description"`
	Videos        []Video `json:"latestVideos"`
	Subscribers   int     `json:"subCount"`
	TotalViews    int     `json:"totalViews"`
}
