package types

type Result struct {
	Type          string `json:"type"`
	Title         string `json:"title"`
	VideoID       string `json:"videoId"`
	Author        string `json:"author"`
	PublishedText string `json:"publishedText"`
	LengthSeconds int    `json:"lengthSeconds"`
	ViewCount     int    `json:"viewCount"`
}