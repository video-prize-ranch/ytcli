package types

type Context struct {
	PrevView   string
	ActionInfo map[string]string
}