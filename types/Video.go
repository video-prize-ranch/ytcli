package types

type Video struct {
	Title         string `json:"title"`
	VideoId       string `json:"videoId"`
	Description   string `json:"description"`
	Views         int    `json:"viewCount"`
	Likes         int    `json:"likeCount"`
	Dislikes      int    `json:"dislikeCount"`
	Author        string `json:"author"`
	AuthorId      string `json:"authorId"`
	LengthSeconds int    `json:"lengthSeconds"`
	PublishedText string `json:"publishedText"`
}