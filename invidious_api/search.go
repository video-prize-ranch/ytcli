package invidious_api

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"time"

	"github.com/imabritishcow/ytcli/types"
	"github.com/imabritishcow/ytcli/utils"
)

func Search(query string) []map[string]string {
	config := utils.GetConfig()

	url := config.InvidiousUrl + "/api/v1/search?fields=title,type,videoId,author,publishedText,lengthSeconds,viewCount&q=" + url.QueryEscape(query)

	httpClient := http.Client{
		Timeout: time.Second * 10,
	}

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0")

	res, getErr := httpClient.Do(req)
	if getErr != nil {
		log.Fatal(getErr)
	}

	if res.Body != nil {
		defer res.Body.Close()
	}

	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		log.Fatal(readErr)
	}

	var results1 []types.Result
	jsonErr := json.Unmarshal(body, &results1)
	if jsonErr != nil {
		log.Fatal(jsonErr)
	}

	results := []map[string]string{}

	for i := 0; i < len(results1); i++ {
		result := results1[i]

		t, err := time.ParseDuration(fmt.Sprint(result.LengthSeconds) + "s")
		if err != nil {
			fmt.Println(err)
		}

		// Invidious API does not return channels or playlists
		video := map[string]string{
			"type": "video",
			"title": result.Title,
			"id": result.VideoID,
			"published": result.PublishedText,
			"views": fmt.Sprint(result.ViewCount),
			"author": result.Author,
			"length": t.String(),
		}

		results = append(results, video)
	}

	return results;
}
