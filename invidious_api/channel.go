package invidious_api

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/imabritishcow/ytcli/types"
	"github.com/imabritishcow/ytcli/utils"
)

func GetChannel(id string) types.Channel {
	config := utils.GetConfig()

	url := config.InvidiousUrl + "/api/v1/channels/"+id+"?fields=author,authorId,description,latestVideos,subCount,totalViews"

	httpClient := http.Client{
		Timeout: time.Second * 10,
	}

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0")

	res, getErr := httpClient.Do(req)
	if getErr != nil {
		log.Fatal(getErr)
	}

	if res.Body != nil {
		defer res.Body.Close()
	}

	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		log.Fatal(readErr)
	}

	var channel types.Channel
	jsonErr := json.Unmarshal(body, &channel)
	if jsonErr != nil {
		log.Fatal(jsonErr)
	}

	return channel;
}
