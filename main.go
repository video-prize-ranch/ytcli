package main

import (
	"github.com/imabritishcow/ytcli/cmd"
	"github.com/spf13/cobra"
)


func main() {
	var rootCmd = &cobra.Command{Use: "app"}

	rootCmd.AddCommand(commands.SearchCmd)
	rootCmd.Execute()
}
