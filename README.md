> ⚠️ **Archived** - Use [ytfzf](https://github.com/pystardust/ytfzf) instead.

# ytcli
A minimal command line client for YouTube.

## Installation

### go get
Requirements:
- Go
- mpv (for playing videos)

```
go get github.com/imabritishcow/ytcli
```

You can now use ytcli!
```
ytcli s [QUERY]
```

### Build from source
Requirements:
- Go
- mpv (for playing videos)

Build the project.
```
go build main.go
```
Move the file to somewhere into your PATH
```
sudo mv main /usr/bin/yt
```
You can now use ytcli!
```
yt s [QUERY]
```

## Usage

### Commands

#### search
Aliases: s
Usage: search [QUERY]

Search YouTube for videos, channels, and playlists. QUERY is optional.

**More commands soon**
