package api

import (
	"log"
	"strconv"
	"strings"

	"github.com/imabritishcow/ytcli/types"
	"github.com/imabritishcow/ytcli/utils"
	"github.com/tidwall/gjson"
)

func GetVideo(id string) types.Video {
	jsonData := utils.GetYtInitialData("https://www.youtube.com/watch?v=" + id)
	playerData := utils.GetYtInitialPlayerResponse("https://www.youtube.com/watch?v=" + id)

	primaryInfo := gjson.Get(jsonData, "contents.twoColumnWatchNextResults.results.results.contents.0.videoPrimaryInfoRenderer")
	playerInfo := gjson.Get(playerData, "microformat.playerMicroformatRenderer")

	likesString := strings.ReplaceAll(primaryInfo.Get("sentimentBar.sentimentBarRenderer.tooltip").String(), ",", "")
	likesDislikes := strings.Split(likesString, " / ")

	likes, likesErr := strconv.Atoi(likesDislikes[0])
	if likesErr != nil {
		log.Fatal(likesErr)
	}

	dislikes, dislikesErr := strconv.Atoi(likesDislikes[1])
	if dislikesErr != nil {
		log.Fatal(likesErr)
	}

	return types.Video{
		Title: primaryInfo.Get("title.runs.0.text").String(),
		VideoId: id,
		Description: playerInfo.Get("description.simpleText").String(),
		Views: int(playerInfo.Get("viewCount").Int()),
		Likes: likes,
		Dislikes: dislikes,
		Author: playerInfo.Get("ownerChannelName").String(),
		AuthorId: gjson.Get(playerData, "videoDetails.channelId").String(),
		LengthSeconds: int(playerInfo.Get("lengthSeconds").Int()),
		PublishedText: primaryInfo.Get("dateText.simpleText").String(),
	}
}
