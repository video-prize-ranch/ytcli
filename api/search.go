package api

import (
	"fmt"
	"net/url"

	"github.com/imabritishcow/ytcli/utils"
	"github.com/tidwall/gjson"
)

func Search(query string) []map[string]string {
	results := make([]map[string]string, 0)

	jsonData := utils.GetYtInitialData("https://www.youtube.com/results?search_query=" + url.QueryEscape(query))

	value := gjson.Get(jsonData, "contents.twoColumnSearchResultsRenderer.primaryContents.sectionListRenderer.contents.0.itemSectionRenderer.contents")
	value.ForEach(func(key gjson.Result, value gjson.Result) bool {
		if value.Get("channelRenderer").Exists() {
			channel := map[string]string{
				"type":        "channel",
				"name":        value.Get("channelRenderer.title.simpleText").String(),
				"id":          value.Get("channelRenderer.channelId").String(),
				"subscribers": value.Get("channelRenderer.subscriberCountText.simpleText").String(),
			}

			results = append(results, channel)
		} else if value.Get("videoRenderer").Exists() {
			video := map[string]string{
				"type":      "video",
				"title":     value.Get("videoRenderer.title.runs.0.text").String(),
				"id":        value.Get("videoRenderer.videoId").String(),
				"published": value.Get("videoRenderer.publishedTimeText.simpleText").String(),
				"views":     value.Get("videoRenderer.viewCountText.simpleText").String(),
				"author":    value.Get("videoRenderer.ownerText.runs.0.text").String(),
				"length":    value.Get("videoRenderer.lengthText.simpleText").String(),
			}

			results = append(results, video)
		} else if value.Get("playlistRenderer").Exists() {
			playlist := map[string]string{
				"type":       "playlist",
				"title":      value.Get("playlistRenderer.title.simpleText").String(),
				"id":         value.Get("playlistRenderer.playlistId").String(),
				"videoCount": fmt.Sprint(value.Get("playlistRenderer.videoCount").Int()),
				"author":     value.Get("playlistRenderer.longBylineText.runs.0.text").String(),
			}

			results = append(results, playlist)
		}
		return true;
	})

	return results;
}
