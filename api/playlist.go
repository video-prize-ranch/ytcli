package api

import (
	"strings"

	"github.com/imabritishcow/ytcli/types"
	"github.com/imabritishcow/ytcli/utils"
	"github.com/tidwall/gjson"
)

func GetPlaylist(id string) types.Playlist {
	jsonData := utils.GetYtInitialData("https://www.youtube.com/playlist?list=" + id)

	metadata := gjson.Get(jsonData, "metadata.playlistMetadataRenderer")
	videosData := gjson.Get(jsonData, "contents.twoColumnBrowseResultsRenderer.tabs.0.tabRenderer.0.content.sectionListRenderer.contents.0.itemSectionRenderer.contents.0.playlistVideoListRenderer.contents")
	
	videos := []types.Video{}
	videosData.ForEach(
		func(key gjson.Result, value gjson.Result) bool {
			videoTitle := value.Get("playlistVideoRenderer.title.runs.0.text").String()

			videos = append(videos, types.Video{
				Title: videoTitle,
				VideoId: value.Get("playlistVideoRenderer.videoId").String(),
				Author: value.Get("playlistVideoRenderer.shortBylineText.runs.0.text").String(),
				AuthorId: value.Get("playlistVideoRenderer.shortBylineText.runs.0.navigationEndpoint.browseEndpoint.browserId").String(),
				LengthSeconds: int(value.Get("playlistVideoRenderer.shortBylineText.runs.0.navigationEndpoint.browseEndpoint.browserId").Int()),
				PublishedText: strings.ReplaceAll(value.Get("playlistVideoRenderer.title.accessibility.accessibilityData.label").String(), videoTitle + " ", ""),
			})

			return true;
		},
	)

	return types.Playlist{
		Title: metadata.Get("title").String(),
		Id: id,
		Description: metadata.Get("description").String(),
		Author: metadata.Get("playlistSidebarSecondaryInfoRenderer.videoOwner.videoOwnerRenderer.title.runs.0.text").String(),
		AuthorId: metadata.Get("playlistSidebarSecondaryInfoRenderer.videoOwner.videoOwnerRenderer.navigationEndpoint.browseEndpoint.browseId").String(),
		VideoCount: int(metadata.Get("sidebar.playlistSidebarRenderer.items.0.playlistSidebarPrimaryInfoRenderer.stats.0.runs.0.text").Int()),
		Videos: videos,
	};
}