package commands

import (
	"strings"

	"github.com/imabritishcow/ytcli/types"
	"github.com/imabritishcow/ytcli/views"
	"github.com/spf13/cobra"
)

var SearchCmd = &cobra.Command{
	Use:   "search [QUERY]",
	Short: "Searches YouTube",
	Long:  "Searches YouTube for channels, videos, and playlists.",
	Aliases: []string{"s"},
	Args:  cobra.MinimumNArgs(0),
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			views.SearchWithPrompt()
		} else {
			query := strings.Join(args, " ")

			views.SearchWithQuery(types.Context{
				PrevView: "none",
				ActionInfo: map[string]string{
					"query": query,
				},
			})
		}
	},
}