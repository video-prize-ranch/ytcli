module github.com/imabritishcow/ytcli

go 1.15

require (
	github.com/PuerkitoBio/goquery v1.6.1
	github.com/atotto/clipboard v0.1.2
	github.com/fatih/color v1.10.0
	github.com/manifoldco/promptui v0.8.0
	github.com/spf13/cobra v1.0.0
	github.com/tidwall/gjson v1.6.8
)
