package views

import (
	"fmt"
	"log"
	"strconv"
	"time"
	"regexp"

	"github.com/fatih/color"
	//	"github.com/imabritishcow/ytcli/api"
	"github.com/imabritishcow/ytcli/invidious_api"
	"github.com/imabritishcow/ytcli/types"
	"github.com/imabritishcow/ytcli/utils"
	"github.com/manifoldco/promptui"
	"github.com/atotto/clipboard"
)

func Channel(context types.Context) {
	config := utils.GetConfig()

	channel := types.Channel{}
	if config.PreferredApi == "invidious" {
		channel = invidious_api.GetChannel(context.ActionInfo["channelId"])
	} else {
		channel = invidious_api.GetChannel(context.ActionInfo["channelId"])
	}

	fmt.Printf(
		"\n" + color.HiBlueString(channel.Name) + "\n" +
		fmt.Sprint(channel.Subscribers) + " subscribers - " + fmt.Sprint(channel.TotalViews) + " total views\n\n" +
		channel.Description,
	)

	prompt := promptui.Select{
		Label: "Action",
		Items: []string{color.BlueString("Videos"), color.BlueString("Subscribe (WIP)"), color.BlueString("Share"), color.BlueString("Back")},
	}

	_, selection, err := prompt.Run()
	if err != nil {
		log.Fatal(err)
	}

	switch selection {
	case color.BlueString("Videos"):
		resultItems := []string{
			"Back",
		}

		channelVideos := invidious_api.GetChannelVideos(context.ActionInfo["channelId"])
		if config.PreferredApi == "invidious" {
			channelVideos = invidious_api.GetChannelVideos(context.ActionInfo["channelId"])
		}

	  for i := 0; i < len(channelVideos); i++ {
			video := channelVideos[i]

			t, err := time.ParseDuration(fmt.Sprint(video.LengthSeconds) + "s")
			if err != nil {
				fmt.Println(err)
			}

			resultItems = append(resultItems, fmt.Sprint(i)+" - "+color.BlueString(video.Title)+" - "+video.Author+" - "+t.String()+" - "+video.PublishedText)
	  }

		videosPrompt := promptui.Select{
			Label: channel.Name + " - Videos",
			Items: resultItems,
			Size:  10,
		}

		_, selection, err := videosPrompt.Run()
		if err != nil {
			log.Fatal(err)
		}

		if selection == "Back" {
			Channel(context)
		}

	  re, _ := regexp.Compile("[0-9]+")

		videoNumString := re.FindString(selection)
		videoNum, err := strconv.Atoi(videoNumString)
		if err != nil {
			log.Fatal(err)
		}

		selectedVideo := channel.Videos[videoNum]

		Video(types.Context{
      PrevView: "Channel",
      ActionInfo: map[string]string{
        "channelId": context.ActionInfo["channelId"],
        "videoId": selectedVideo.VideoId,
      },
    })
	case color.BlueString("Subscribe (WIP)"):
		fmt.Println("Not implemented.")
	case color.BlueString("Share"):
		if clipboard.Unsupported {
			fmt.Println("Unable to copy link to clipboard, unsupported.")
			fmt.Println("https://youtube.com/channel/" + channel.ChannelId)
		} else {
			clipErr := clipboard.WriteAll("https://youtube.com/channel/" + channel.ChannelId)
			if clipErr != nil {
				log.Fatal(clipErr)
			}

			fmt.Println("Copied link to clipboard!")
		}
	case color.BlueString("Back"):
		switch context.PrevView {
		case "SearchWithQuery":
			SearchWithQuery(types.Context{
				PrevView: "Video",
				ActionInfo: map[string]string{
					"query": context.ActionInfo["query"],
				},
			})
		case "Video":
			Video(types.Context{
				PrevView: "Channel",
				ActionInfo: map[string]string{
					"videoId": context.ActionInfo["videoId"],
				},
			})
		}
	}
}
