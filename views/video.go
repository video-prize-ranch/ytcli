package views

import (
	"fmt"
	"log"
	"os/exec"

	"github.com/fatih/color"
	"github.com/imabritishcow/ytcli/api"
	"github.com/imabritishcow/ytcli/invidious_api"
	"github.com/imabritishcow/ytcli/types"
	"github.com/imabritishcow/ytcli/utils"
	"github.com/manifoldco/promptui"
	"github.com/atotto/clipboard"
)

func Video(context types.Context) {
	config := utils.GetConfig()

	video := types.Video{}
	if config.PreferredApi == "invidious" {
		video = invidious_api.GetVideo(context.ActionInfo["videoId"])
	} else {
		video = api.GetVideo(context.ActionInfo["videoId"])
	}

	fmt.Printf(
		"\n" + color.HiBlueString(video.Title) + "\n" +
		video.Author + " - " + video.PublishedText + " - " + fmt.Sprint(video.Views) + " views\n" +
		"👍 " + fmt.Sprint(video.Likes) + " 👎 " + fmt.Sprint(video.Dislikes) + "\n\n" +
		video.Description + "\n\n" +
		"Video link: https://youtube.com/watch?v=" + video.VideoId,
	)

	prompt := promptui.Select{
		Label: "Action",
		Items: []string{
			color.BlueString("Play"), 
			color.BlueString("View channel"), 
			color.BlueString("Subscribe (WIP)"), 
			color.BlueString("Share"), 
			color.BlueString("Back"),
		},
	}

	_, selection, err := prompt.Run()
	if err != nil {
		log.Fatal(err)
	}

	switch selection {
	case color.BlueString("Play"):
		fmt.Printf("Opening mpv...")

		out, err := exec.Command("mpv", "--fs", "https://youtu.be/" + video.VideoId).Output()
		if err != nil {
			fmt.Printf("%s\n", err)
		}
		fmt.Println(string(out))
	case color.BlueString("View channel"):
		Channel(types.Context{
			PrevView: "Video",
			ActionInfo: map[string]string{
				"videoId": video.VideoId,
				"channelId": video.AuthorId,
			},
		})
	case color.BlueString("Subscribe (WIP)"):
		fmt.Println("Not implemented.")
	case color.BlueString("Share"):
		if clipboard.Unsupported {
			fmt.Println("Unable to copy link to clipboard, unsupported.")
			fmt.Println("https://youtube.com/watch?v=" + video.VideoId)
		} else {
			clipErr := clipboard.WriteAll("https://youtube.com/watch?v=" + video.VideoId)
			if clipErr != nil {
				log.Fatal(clipErr)
			}

			fmt.Println("Copied link to clipboard!")
		}
	}

	switch context.PrevView {
	case "SearchWithQuery":
		SearchWithQuery(types.Context{
			PrevView: "Video",
			ActionInfo: map[string]string{
				"query": context.ActionInfo["query"],
			},
		})
	case "Channel":
		Channel(types.Context{
			PrevView: "Video",
			ActionInfo: map[string]string{
				"videoId": video.VideoId,
				"channelId": context.ActionInfo["channelId"],
			},
		})
	}
}