package views

import (
	"fmt"
	"log"
	"regexp"
	"strconv"

	"github.com/fatih/color"
	"github.com/imabritishcow/ytcli/api"
	"github.com/imabritishcow/ytcli/invidious_api"
	"github.com/imabritishcow/ytcli/types"
	"github.com/imabritishcow/ytcli/utils"
	"github.com/manifoldco/promptui"
)

func SearchWithQuery(context types.Context) {
	config := utils.GetConfig()

	results := api.Search(context.ActionInfo["query"])
	if config.PreferredApi == "invidious" {
		results = invidious_api.Search(context.ActionInfo["query"])
	}

	resultItems := []string{
		"🔎 Search again",
	}

	for i := 0; i < len(results); i++ {
		result := results[i]

		switch result["type"] {
		case "channel":
			resultItems = append(resultItems, fmt.Sprint(i)+" - "+color.BlueString(result["name"])+" - "+result["subscribers"])
		case "playlist":
			resultItems = append(resultItems, fmt.Sprint(i)+" - "+color.BlueString(result["title"])+" - "+result["author"]+" - "+result["videoCount"]+" videos")
		case "video":
			resultItems = append(resultItems, fmt.Sprint(i)+" - "+color.BlueString(result["title"])+" - "+result["author"]+" - "+result["length"]+" - "+result["published"])
		}
	}

	searchPrompt := promptui.Select{
		Label: "Results",
		Items: resultItems,
		Size:  10,
	}

	_, selection, err := searchPrompt.Run()
	if err != nil {
		log.Fatal(err)
	}

	if selection == "🔎 Search again" {
		SearchWithPrompt()
	}

	re, _ := regexp.Compile("[0-9]+")

	resultIDString := re.FindString(selection)
	resultID, err := strconv.Atoi(resultIDString)
	if err != nil {
		log.Fatal(err)
	}

	selectedResult := results[resultID]

	switch selectedResult["type"] {
	case "channel":
		Channel(types.Context{
			PrevView: "SearchWithQuery",
			ActionInfo: map[string]string{
				"query": context.ActionInfo["query"],
				"channelId": selectedResult["id"],
			},
		})
	case "playlist":
		fmt.Println("Playlists are currently not supported.")

		SearchWithQuery(context)
	case "video":
		Video(types.Context{
			PrevView: "SearchWithQuery",
			ActionInfo: map[string]string{
				"query": context.ActionInfo["query"],
				"videoId": selectedResult["id"],
			},
		})
	}
}

func SearchWithPrompt() {
	prompt := promptui.Prompt{
		Label:    "Search",
	}

	query, err := prompt.Run()
	if err != nil {
		log.Fatal(err)
	}

	SearchWithQuery(types.Context{
		PrevView: "SearchWithPrompt",
		ActionInfo: map[string]string{
			"query": query,
		},
	})
}