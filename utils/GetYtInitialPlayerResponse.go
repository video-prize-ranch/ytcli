package utils

import (
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
)

func GetYtInitialPlayerResponse(url string) string {
	httpClient := http.Client{
		Timeout: time.Second * 10,
	}

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		log.Fatal(err)
	}

	req.Header.Set("User-Agent", "Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0")

	res, getErr := httpClient.Do(req)
	if getErr != nil {
		log.Fatal(getErr)
	}

	document, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		log.Fatal("Error loading HTTP response body. ", err)
	}

	jsonData := ""

	document.Find("script").Each(
		func(index int, element *goquery.Selection) {
			htmlString := element.Text()

			if strings.Contains(htmlString, "var ytInitialPlayerResponse = ") {
				jsonData = strings.ReplaceAll(htmlString, "var ytInitialPlayerResponse = ", "")
			}
		},
	)

	return jsonData;
}