package utils

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
)

type Config struct {
	PreferredApi string `json:"PREFERRED_API"`
	InvidiousUrl string `json:"INVIDIOUS_URL"`
}

func GetConfig() Config {
	home, err := os.UserHomeDir()
	if err != nil {
		log.Fatal(err)
	}

	configFilePath := home + "/.config/ytcli/config.json"

	if _, err := os.Stat(configFilePath); os.IsNotExist(err) {
		defaultConfig := `
		{
			"PREFERRED_API": "local",
			"INVIDIOUS_URL": "https://invidious.snopyta.org"
		}
		`

		os.Mkdir(home + "/.config/ytcli/", 0777)
		err := ioutil.WriteFile(configFilePath, []byte(defaultConfig), 0666)
		if err != nil {
			log.Fatal(err)
		}

		return Config{
			PreferredApi: "local",
			InvidiousUrl: "https://invidious.snopyta.org",
		};
	} else {
		configFile, err := ioutil.ReadFile(configFilePath)
		if err != nil {
			log.Fatal(err)
		}

		var config Config

		jsonErr := json.Unmarshal(configFile, &config)
		if jsonErr != nil {
			log.Fatal(jsonErr)
		}

		return config;
	}
}
